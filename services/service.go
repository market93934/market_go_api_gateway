package services

import (
	"gitlab.com/market93934/market_go_api_gateway/config"
	"gitlab.com/market93934/market_go_api_gateway/genproto/arrival_service"
	"gitlab.com/market93934/market_go_api_gateway/genproto/product_service"
	"gitlab.com/market93934/market_go_api_gateway/genproto/sales_service"
	"gitlab.com/market93934/market_go_api_gateway/genproto/user_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	StaffService() user_service.StaffServiceClient
	ProviderService() user_service.ProviderServiceClient
	BranchService() user_service.BranchServiceClient
	MarketService() user_service.MarketServiceClient
	CategoryService() product_service.CategoryServiceClient
	ReminderService() product_service.ReminderServiceClient
	ProductService() product_service.ProductServiceClient
	BrandService() product_service.BrandServiceClient
	ArrivalService() arrival_service.ArrivalServiceClient
	ArrivalProductService() arrival_service.ArrivalProductServiceClient
	SaleService() sales_service.SalesServiceClient
	SaleProductService() sales_service.SalesProductServiceClient
	ShiftService() sales_service.ShiftServiceClient
	PaymentService() sales_service.PaymentServiceClient
	TransactionService() sales_service.TransactionServiceClient
}

type grpcClients struct {
	staffService          user_service.StaffServiceClient
	providerService       user_service.ProviderServiceClient
	branchService         user_service.BranchServiceClient
	marketService         user_service.MarketServiceClient
	categoryService       product_service.CategoryServiceClient
	reminderService       product_service.ReminderServiceClient
	productService        product_service.ProductServiceClient
	brandService          product_service.BrandServiceClient
	arrivalService        arrival_service.ArrivalServiceClient
	arrivalProductService arrival_service.ArrivalProductServiceClient
	saleService           sales_service.SalesServiceClient
	saleProductService    sales_service.SalesProductServiceClient
	shiftService          sales_service.ShiftServiceClient
	paymentService        sales_service.PaymentServiceClient
	transactionService    sales_service.TransactionServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// connection to Order Microservice

	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}
	connProviderService, err := grpc.Dial(cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connBranchService, err := grpc.Dial(cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connMarketService, err := grpc.Dial(cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connCategoryService, err := grpc.Dial(cfg.PRODUCTServiceHost+cfg.PRODUCTGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connReminderService, err := grpc.Dial(cfg.PRODUCTServiceHost+cfg.PRODUCTGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connProductService, err := grpc.Dial(cfg.PRODUCTServiceHost+cfg.PRODUCTGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connBrandService, err := grpc.Dial(cfg.PRODUCTServiceHost+cfg.PRODUCTGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connArrvivalService, err := grpc.Dial(cfg.ARRIVALServiceHost+cfg.ARRIVALGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connArrivalProductService, err := grpc.Dial(cfg.ARRIVALServiceHost+cfg.ARRIVALGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connSaleService, err := grpc.Dial(cfg.SALEServiceHost+cfg.SALEGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connSaleProductService, err := grpc.Dial(cfg.SALEServiceHost+cfg.SALEGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connShiftService, err := grpc.Dial(cfg.SALEServiceHost+cfg.SALEGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connPaymentService, err := grpc.Dial(cfg.SALEServiceHost+cfg.SALEGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	connTransactionService, err := grpc.Dial(cfg.SALEServiceHost+cfg.SALEGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	return &grpcClients{
		staffService:          user_service.NewStaffServiceClient(connUserService),
		providerService:       user_service.NewProviderServiceClient(connProviderService),
		branchService:         user_service.NewBranchServiceClient(connBranchService),
		marketService:         user_service.NewMarketServiceClient(connMarketService),
		categoryService:       product_service.NewCategoryServiceClient(connCategoryService),
		reminderService:       product_service.NewReminderServiceClient(connReminderService),
		productService:        product_service.NewProductServiceClient(connProductService),
		brandService:          product_service.NewBrandServiceClient(connBrandService),
		arrivalService:        arrival_service.NewArrivalServiceClient(connArrvivalService),
		arrivalProductService: arrival_service.NewArrivalProductServiceClient(connArrivalProductService),
		saleService:           sales_service.NewSalesServiceClient(connSaleService),
		saleProductService:    sales_service.NewSalesProductServiceClient(connSaleProductService),
		shiftService:          sales_service.NewShiftServiceClient(connShiftService),
		paymentService:        sales_service.NewPaymentServiceClient(connPaymentService),
		transactionService:    sales_service.NewTransactionServiceClient(connTransactionService),
	}, nil
}

func (g *grpcClients) StaffService() user_service.StaffServiceClient {
	return g.staffService
}

func (g *grpcClients) ProviderService() user_service.ProviderServiceClient {
	return g.providerService
}
func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branchService
}
func (g *grpcClients) MarketService() user_service.MarketServiceClient {
	return g.marketService
}
func (g *grpcClients) CategoryService() product_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ReminderService() product_service.ReminderServiceClient {
	return g.reminderService
}
func (g *grpcClients) ProductService() product_service.ProductServiceClient {
	return g.productService
}

func (g *grpcClients) BrandService() product_service.BrandServiceClient {
	return g.brandService
}
func (g *grpcClients) ArrivalService() arrival_service.ArrivalServiceClient {
	return g.arrivalService
}
func (g *grpcClients) ArrivalProductService() arrival_service.ArrivalProductServiceClient {
	return g.arrivalProductService
}
func (g *grpcClients) SaleService() sales_service.SalesServiceClient {
	return g.saleService
}
func (g *grpcClients) SaleProductService() sales_service.SalesProductServiceClient {
	return g.saleProductService
}
func (g *grpcClients) ShiftService() sales_service.ShiftServiceClient {
	return g.shiftService
}
func (g *grpcClients) PaymentService() sales_service.PaymentServiceClient {
	return g.paymentService
}
func (g *grpcClients) TransactionService() sales_service.TransactionServiceClient {
	return g.transactionService
}
