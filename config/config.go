package config

import (
	"fmt"
	"os"
	"time"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

const (
	// DebugMode indicates service mode is debug.
	DebugMode = "debug"
	// TestMode indicates service mode is test.
	TestMode = "test"
	// ReleaseMode indicates service mode is release.
	ReleaseMode = "release"

	TimeExpiredAt = time.Hour * 720
)

type Config struct {
	ServiceName string
	Environment string // debug, test, release
	Version     string

	ServisHost string
	HTTPPort   string
	HTTPScheme string
	Domain     string

	DefaultOffset     string
	DefaultLimit      string
	DefaultSaleId     string
	DefaultFrom       string
	DefaultTo         string
	DefaultBarCode    string
	DefaultName       string
	DefaultBranchId   string
	DefaultArrivalId  string
	DefaultBrandId     string
	DefaultCategoryId string
	DefaultShiftId    string
	DefaultStaffId    string
	DefaultMarketId   string
	DefaultLastName string

	//ecommerce_go_order_service host and port for connection to setcive
	UserServiceHost string
	UserGRPCPort    string

	PRODUCTServiceHost string
	PRODUCTGRPCPort    string

	ARRIVALServiceHost string
	ARRIVALGRPCPort    string

	SALEServiceHost string
	SALEGRPCPort    string

	SecretKey string
}

// Load ...
func Load() Config {
	if err := godotenv.Load("./.env"); err != nil {
		fmt.Println("No .env file found")
	}

	config := Config{}

	config.ServiceName = cast.ToString(getOrReturnDefaultValue("SERVICE_NAME", "market_go_api_gateway"))
	config.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", DebugMode))
	config.Version = cast.ToString(getOrReturnDefaultValue("VERSION", "1.0"))

	config.ServisHost = cast.ToString(getOrReturnDefaultValue("SERVICE_HOST", "localhost"))
	config.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":8001"))
	config.HTTPScheme = cast.ToString(getOrReturnDefaultValue("HTTP_SCHEME", "http"))
	config.Domain = cast.ToString(getOrReturnDefaultValue("DOMAIN", "localhost:8001"))

	config.DefaultOffset = cast.ToString(getOrReturnDefaultValue("OFFSET", "0"))
	config.DefaultLimit = cast.ToString(getOrReturnDefaultValue("LIMIT", "0"))
	config.DefaultSaleId = cast.ToString(getOrReturnDefaultValue("SALE_IF", ""))
	config.DefaultBarCode = cast.ToString(getOrReturnDefaultValue("BAR_CODE", ""))
	config.DefaultName = cast.ToString(getOrReturnDefaultValue("NAME", ""))
	config.DefaultBranchId = cast.ToString(getOrReturnDefaultValue("BRANCH_ID", ""))
	config.DefaultArrivalId = cast.ToString(getOrReturnDefaultValue("ARRIVAL_ID", ""))
	config.DefaultBrandId = cast.ToString(getOrReturnDefaultValue("BRAND_ID", ""))
	config.DefaultCategoryId = cast.ToString(getOrReturnDefaultValue("CATEGORY", ""))
	config.DefaultShiftId = cast.ToString(getOrReturnDefaultValue("SHIFT_ID", ""))
	config.DefaultStaffId = cast.ToString(getOrReturnDefaultValue("STAFF_ID", ""))
	config.DefaultMarketId = cast.ToString(getOrReturnDefaultValue("MARKET_ID", ""))
	config.DefaultLastName = cast.ToString(getOrReturnDefaultValue("LAST_NAME", ""))


	config.UserServiceHost = cast.ToString(getOrReturnDefaultValue("USER_SERVICE_HOST", "localhost"))
	config.UserGRPCPort = cast.ToString(getOrReturnDefaultValue("USER_GRPC_PORT", ":9101"))

	config.PRODUCTServiceHost = cast.ToString(getOrReturnDefaultValue("PRODUCT_SERVICE_HOST", "localhost"))
	config.PRODUCTGRPCPort = cast.ToString(getOrReturnDefaultValue("PRODUCT_GRPC_PORT", ":9102"))

	config.ARRIVALServiceHost = cast.ToString(getOrReturnDefaultValue("ARRIVAL_SERVICE_HOST", "localhost"))
	config.ARRIVALGRPCPort = cast.ToString(getOrReturnDefaultValue("ARRIVAL_GRPC_PORT", ":9103"))

	config.SALEServiceHost = cast.ToString(getOrReturnDefaultValue("SALE_SERVICE_HOST", "localhost"))
	config.SALEGRPCPort = cast.ToString(getOrReturnDefaultValue("SALE_GRPC_PORT", ":9104"))

	config.SecretKey = cast.ToString(getOrReturnDefaultValue("SECRET_KEY", "market"))

	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
