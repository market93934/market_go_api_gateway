package handlers

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/sales_service"
)

// @Security ApiKeyAuth
// CreateShift godoc
// @ID create_shift
// @Router /v1/shift [POST]
// @Summary  Create Shift
// @Description Create Shift
// @Tags Shift
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body sales_service.CreateShift true "CreateShiftRequestBody"
// @Success 200 {object} http.Response{data=sales_service.Shift} "GetShiftBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateShift(c *gin.Context) {
	var shift sales_service.CreateShift

	err := c.ShouldBindJSON(&shift)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ShiftService().Create(c, &shift)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	transaction, err := h.services.TransactionService().Create(c, &sales_service.CreateTransaction{
		Cash:    0,
		Uzcard:  0,
		Payme:   0,
		Click:   0,
		Humo:    0,
		Apelsin: 0,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	fmt.Println("id from transaction", transaction.Id)
	fmt.Println("transaction id from shift ", resp.TransactionId)
	_, err = h.services.ShiftService().Update(c, &sales_service.UpdateShift{
		Id:            resp.Id,
		BranchId:      resp.BranchId,
		ProviderId:    resp.ProviderId,
		MarketId:      resp.MarketId,
		Status:        resp.Status,
		TransactionId: transaction.Id,
		StaffId:       resp.StaffId,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	info, err := h.services.ShiftService().GetByID(c, &sales_service.ShiftPrimaryKey{
		Id: resp.Id,
	})
	fmt.Println("transaction id in shift after updating ", info.TransactionId)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, info)

}

// @Security ApiKeyAuth
// GetShiftByID godoc
// @ID get_shift_by_id
// @Router /v1/shift/{id} [GET]
// @Summary Get Shift By ID
// @Description Get Shift By ID
// @Tags Shift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=sales_service.Shift} "ShiftBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShiftById(c *gin.Context) {

	shiftId := c.Param("id")

	resp, err := h.services.ShiftService().GetByID(c, &sales_service.ShiftPrimaryKey{Id: shiftId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetShiftList godoc
// @ID get_shift_list
// @Router /v1/shift [GET]
// @Summary Get Shift s List
// @Description  Get Shift s List
// @Tags Shift
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param shift_id query string false "shift_id"
// @Param branch_id query string false "branch_id"
// @Param staff_id query string false "staff_id"
// @Param market_id query string false "market_id"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=sales_service.GetListShiftResponse} "GetAllShiftResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShiftList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	shift_id, err := h.getShiftIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	branch_id, err := h.getBranchIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	staff_id, err := h.getStaffIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	market_id, err := h.getmarketIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ShiftService().GetList(
		context.Background(),
		&sales_service.GetListShiftRequest{
			Limit:          int64(limit),
			Offset:         int64(offset),
			SearchShiftId:  shift_id,
			SearchBranchId: branch_id,
			SearchStaffId:  staff_id,
			SearchMarketId: market_id,
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateShift godoc
// @ID update_shift
// @Router /v1/shift/{id} [PUT]
// @Summary Update Shift
// @Description Update Shift
// @Tags Shift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body sales_service.UpdateShift true "UpdateShiftRequestBody"
// @Success 200 {object} http.Response{data=sales_service.Shift} "Shift data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateShift(c *gin.Context) {
	var shift sales_service.UpdateShift

	shift.Id = c.Param("id")

	err := c.ShouldBindJSON(&shift)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ShiftService().Update(
		c.Request.Context(),
		&shift,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteShift godoc
// @ID delete_shift
// @Router /v1/shift/{id} [DELETE]
// @Summary Delete Shift
// @Description Delete Shift
// @Tags Shift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Shift data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteShift(c *gin.Context) {

	shiftId := c.Param("id")

	resp, err := h.services.ShiftService().Delete(
		c.Request.Context(),
		&sales_service.ShiftPrimaryKey{Id: shiftId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
