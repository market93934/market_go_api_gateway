package handlers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/arrival_service"
	"gitlab.com/market93934/market_go_api_gateway/genproto/product_service"
)

// @Security ApiKeyAuth
// MakeArrival godoc
// @ID make_arrival
// @Router /v1/make_arrival/{id} [GET]
// @Summary MakeArrival
// @Description MakeArrival
// @Tags MakeArrival
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data =string} "Success"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) MakeArrival(c *gin.Context) {
	var (
		existReminder = false
	)

	arrivalId := c.Param("id")
	arrivalProducts, err := h.services.ArrivalProductService().GetList(c, &arrival_service.GetListArrivalProductRequest{})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	fmt.Println("this is arrival products  ", arrivalProducts)
	resp, err := h.services.ArrivalService().GetByID(c, &arrival_service.ArrivalPrimaryKey{
		Id: string(arrivalId),
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	for _, value := range arrivalProducts.ArrivalProducts {
		if value.ArrivalId == resp.Id {
			product, err := h.services.ProductService().GetByID(c, &product_service.ProductPrimaryKey{
				Id: value.ProductId,
			})
			if err != nil {
				c.JSON(http.StatusBadRequest, map[string]interface{}{
					"status":  "GRPC ERROR",
					"message": err.Error(),
				})
				return
			}
			_, err = h.services.ProductService().Update(c.Request.Context(), &product_service.UpdateProduct{
				Id:         product.Id,
				Photo:      product.Photo,
				Name:       product.Name,
				CategotyId: product.CategotyId,
				BrandId:    product.BrandId,
				BarCode:    value.BarCode,
				Price:      value.Price,
			})
			if err != nil {
				c.JSON(http.StatusBadRequest, map[string]interface{}{
					"status":  "GRPC ERROR",
					"message": err.Error(),
				})
				return
			}
			reminderResponse, err := h.services.ReminderService().GetList(c, &product_service.GetListReminderRequest{
				Offset:   0,
				Limit:    0,
				BarCode:  value.BarCode,
				BranchId: resp.BranchId,
			})
			if len(reminderResponse.Reminders) > 0 {
				fmt.Println("this is reminder response", reminderResponse)
				for _, val := range reminderResponse.Reminders {
					if val.ProductId == value.ProductId {
						existReminder = true
					}
				}
			}
			if err != nil {
				c.JSON(http.StatusBadRequest, map[string]interface{}{
					"status":  "GRPC ERROR",
					"message": err.Error(),
				})
				return
			}

			//updates if reminder exists
			if existReminder == true {
				reminder, err := h.services.ReminderService().GetByID(c, &product_service.ReminderPrimaryKey{
					ProductId: value.ProductId,
				})
				if err != nil {
					c.JSON(http.StatusBadRequest, map[string]interface{}{
						"status":  "GRPC ERROR",
						"message": err.Error(),
					})
					return
				}
				_, err = h.services.ReminderService().Update(c, &product_service.UpdateReminder{
					ProductId:  value.ProductId,
					BranchId:   resp.BranchId,
					CategotyId: value.CategoryId,
					BrandId:    value.BrandId,
					BarCode:    value.BarCode,
					Count:      reminder.Count + value.Count,
					Price:      value.Price,
				})
				if err != nil {
					c.JSON(http.StatusBadRequest, map[string]interface{}{
						"status":  "GRPC ERROR",
						"message": err.Error(),
					})
					return
				}
				//creates new reminder if does not exist
			} else {
				_, err = h.services.ReminderService().Create(c, &product_service.CreateReminder{
					BranchId:   resp.BranchId,
					CategotyId: value.CategoryId,
					BrandId:    value.BrandId,
					ProductId:  value.ProductId,
					BarCode:    value.BarCode,
					Count:      value.Count,
					Price:      value.Price,
				})
				if err != nil {
					c.JSON(http.StatusBadRequest, map[string]interface{}{
						"status":  "GRPC ERROR",
						"message": err.Error(),
					})
					return
				}
			}
		}
	}
	c.JSON(http.StatusCreated, "made arrival")

}
