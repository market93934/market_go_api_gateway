package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/user_service"
)

// @Security ApiKeyAuth
// CreateStaff godoc
// @ID create_staff
// @Router /v1/staff [POST]
// @Summary  Create Staff
// @Description Create Staff
// @Tags Staff
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body user_service.CreateStaff true "CreateStaffRequestBody"
// @Success 200 {object} http.Response{data=user_service.Staff} "GetStaffBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateStaff(c *gin.Context) {
	var staff user_service.CreateStaff

	err := c.ShouldBindJSON(&staff)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.StaffService().Create(c, &staff)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetStaffByID godoc
// @ID get_staff_by_id
// @Router /v1/staff/{id} [GET]
// @Summary Get Staff By ID
// @Description Get Staff By ID
// @Tags Staff
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.Staff} "StaffBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetStaffById(c *gin.Context) {

	staffId := c.Param("id")

	resp, err := h.services.StaffService().GetByID(c, &user_service.StaffPrimaryKey{Id: staffId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetStaffList godoc
// @ID get_staff_list
// @Router /v1/staff [GET]
// @Summary Get Staff s List
// @Description  Get Staff s List
// @Tags Staff
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param name query string false "name"
// @Param last_name query string false "last_name"
// @Param phone_number query string false "phone_number"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=user_service.GetListStaffResponse} "GetAllStaffResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetStaffList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	name, err := h.getDefaultNameParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	last_name, err := h.getLastNameParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	phone_number, err := h.getPhoneNumberParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.StaffService().GetList(
		context.Background(),
		&user_service.GetListStaffRequest{
			Limit:             int64(limit),
			Offset:            int64(offset),
			SearchName:        name,
			SearchLastName:    last_name,
			SearchPhoneNumber: phone_number,
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateStaff godoc
// @ID update_staff
// @Router /v1/staff/{id} [PUT]
// @Summary Update Staff
// @Description Update Staff
// @Tags Staff
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateStaff true "UpdateStaffRequestBody"
// @Success 200 {object} http.Response{data=user_service.Staff} "Staff data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateStaff(c *gin.Context) {
	var staff user_service.UpdateStaff

	staff.Id = c.Param("id")

	err := c.ShouldBindJSON(&staff)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.StaffService().Update(
		c.Request.Context(),
		&staff,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteStaff godoc
// @ID delete_staff
// @Router /v1/staff/{id} [DELETE]
// @Summary Delete Staff
// @Description Delete Staff
// @Tags Staff
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Staff data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteStaff(c *gin.Context) {

	staffId := c.Param("id")

	resp, err := h.services.StaffService().Delete(
		c.Request.Context(),
		&user_service.StaffPrimaryKey{Id: staffId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
