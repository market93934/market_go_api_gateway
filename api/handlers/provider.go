package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/user_service"
)

// @Security ApiKeyAuth
// CreateProvider godoc
// @ID create_provider
// @Router /v1/provider [POST]
// @Summary  Create Provider
// @Description Create Provider
// @Tags Provider
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body user_service.CreateProvider true "CreateProviderRequestBody"
// @Success 200 {object} http.Response{data=user_service.Provider} "GetProviderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateProvider(c *gin.Context) {
	var provider user_service.CreateProvider

	err := c.ShouldBindJSON(&provider)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ProviderService().Create(c, &provider)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetProviderByID godoc
// @ID get_provider_by_id
// @Router /v1/provider/{id} [GET]
// @Summary Get Provider By ID
// @Description Get Provider By ID
// @Tags Provider
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.Provider} "ProviderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetProviderById(c *gin.Context) {

	providerId := c.Param("id")

	resp, err := h.services.ProviderService().GetByID(c, &user_service.ProviderPrimaryKey{Id: providerId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetProviderList godoc
// @ID get_provider_list
// @Router /v1/provider [GET]
// @Summary Get Provider s List
// @Description  Get Provider s List
// @Tags Provider
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param name query string false "name"
// @Param phone_number query string false "phone_number"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=user_service.GetListProviderResponse} "GetAllProviderResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetProviderList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	name, err := h.getDefaultNameParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	phone_number, err := h.getPhoneNumberParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ProviderService().GetList(
		context.Background(),
		&user_service.GetListProviderRequest{
			Limit:             int64(limit),
			Offset:            int64(offset),
			SearchName:        name,
			SearchPhoneNumber: phone_number,
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateProvider godoc
// @ID update_provider
// @Router /v1/provider/{id} [PUT]
// @Summary Update Provider
// @Description Update Provider
// @Tags Provider
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateProvider true "UpdateProviderRequestBody"
// @Success 200 {object} http.Response{data=user_service.Provider} "Provider data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateProvider(c *gin.Context) {
	var provider user_service.UpdateProvider

	provider.Id = c.Param("id")

	err := c.ShouldBindJSON(&provider)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ProviderService().Update(
		c.Request.Context(),
		&provider,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteProvider godoc
// @ID delete_provider
// @Router /v1/provider/{id} [DELETE]
// @Summary Delete Provider
// @Description Delete Provider
// @Tags Provider
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Provider data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteProvider(c *gin.Context) {

	providerId := c.Param("id")

	resp, err := h.services.ProviderService().Delete(
		c.Request.Context(),
		&user_service.ProviderPrimaryKey{Id: providerId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
