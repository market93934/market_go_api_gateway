package handlers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/product_service"
	"gitlab.com/market93934/market_go_api_gateway/genproto/sales_service"
)

// @Security ApiKeyAuth
// MakeSale godoc
// @ID make_sale
// @Router /v1/make_sale/{id} [GET]
// @Summary MakeSale
// @Description MakeSale
// @Tags MakeSale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data =string} "Success"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h Handler) MakeSale(c *gin.Context) {
	var (
		NewCash    float64
		NewUzcard  float64
		NewPayme   float64
		NewClick   float64
		NewHumo    float64
		NewApelsin float64
	)
	makesaleId := c.Param("id")

	sales, err := h.services.SaleService().GetByID(c, &sales_service.SalesPrimaryKey{
		Id: string(makesaleId),
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	saleProducts, err := h.services.SaleProductService().GetList(c, &sales_service.GetListSalesProductRequest{
		Limit:  0,
		Offset: 0,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	fmt.Println("saleProducts GetList-->saleId", saleProducts)
	for _, value := range saleProducts.SalesProducts {
		if value.SaleId == sales.Id {
			reminder, err := h.services.ReminderService().GetByID(c, &product_service.ReminderPrimaryKey{
				ProductId: value.ProductId,
			})
			if err != nil {
				c.JSON(http.StatusBadRequest, map[string]interface{}{
					"status":  "GRPC ERROR",
					"message": err.Error(),
				})
				return
			}
			_, err = h.services.ReminderService().Update(c, &product_service.UpdateReminder{
				BranchId:   reminder.BranchId,
				CategotyId: reminder.CategotyId,
				BrandId:    reminder.BrandId,
				ProductId:  reminder.ProductId,
				BarCode:    reminder.BarCode,
				Count:      reminder.Count - value.Count,
				Price:      reminder.Price,
			})
			if err != nil {
				c.JSON(http.StatusBadRequest, map[string]interface{}{
					"status":  "GRPC ERROR",
					"message": err.Error(),
				})
				return
			}
			payment, err := h.services.PaymentService().GetByID(c, &sales_service.PaymentPrimaryKey{
				Id: sales.PaymentId,
			})

			if err != nil {
				c.JSON(http.StatusBadRequest, map[string]interface{}{
					"status":  "GRPC ERROR",
					"message": err.Error(),
				})
				return
			}
			shift, err := h.services.ShiftService().GetByID(c, &sales_service.ShiftPrimaryKey{
				Id: sales.ShiftId,
			})
			if err != nil {

				c.JSON(http.StatusBadRequest, map[string]interface{}{
					"status":  "GRPC ERROR",
					"message": err.Error(),
				})
				return
			}
			transaction, err := h.services.TransactionService().GetByID(c, &sales_service.TransactionPrimaryKey{
				Id: shift.TransactionId,
			})
			if err != nil {
				c.JSON(http.StatusBadRequest, map[string]interface{}{
					"status":  "GRPC ERROR",
					"message": err.Error(),
				})
				return
			}
			NewCash = transaction.Cash + payment.Cash
			NewUzcard = transaction.Uzcard + payment.Uzcard
			NewPayme = transaction.Payme + payment.Payme
			NewClick = transaction.Click + payment.Click
			NewHumo = transaction.Humo + payment.Humo
			NewApelsin = transaction.Apelsin + payment.Apelsin
			fmt.Println("new Cash", NewCash)
			fmt.Println("new Uzcard", NewUzcard)
			fmt.Println("new Payme", NewPayme)
			fmt.Println("new Click", NewClick)
			fmt.Println("new Humo", NewHumo)
			fmt.Println("new Apelsin", NewApelsin)
			_, err = h.services.TransactionService().Update(c, &sales_service.UpdateTransaction{
				Id:      transaction.Id,
				Cash:    NewCash,
				Uzcard:  NewUzcard,
				Payme:   NewPayme,
				Click:   NewClick,
				Humo:    NewHumo,
				Apelsin: NewApelsin,
			})
			if err != nil {
				c.JSON(http.StatusBadRequest, map[string]interface{}{
					"status":  "GRPC ERROR",
					"message": err.Error(),
				})
				return
			}
			fmt.Println("transaction Cash", transaction.Cash)
			fmt.Println("transaction Uzcard", transaction.Uzcard)
			fmt.Println("transaction Payme", transaction.Payme)
			fmt.Println("transaction Click", transaction.Click)
			fmt.Println("transaction Humo", transaction.Humo)
			fmt.Println("transaction Apelsin", transaction.Apelsin)

		}
	}
	_, err = h.services.SaleService().Update(c, &sales_service.UpdateSales{
		Id:        sales.Id,
		BranchId:  sales.BranchId,
		ShiftId:   sales.ShiftId,
		MarketId:  sales.MarketId,
		StaffId:   sales.StaffId,
		Status:    "finished",
		PaymentId: sales.PaymentId,
	})
	c.JSON(http.StatusOK, "successful sale!!")
}
