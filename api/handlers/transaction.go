package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/sales_service"
)

// @Security ApiKeyAuth
// CreateTransaction godoc
// @ID create_transaction
// @Router /v1/transaction [POST]
// @Summary  Create Transaction
// @Description Create Transaction
// @Tags Transaction
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body sales_service.CreateTransaction true "CreateTransactionRequestBody"
// @Success 200 {object} http.Response{data=sales_service.Transaction} "GetTransactionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateTransaction(c *gin.Context) {
	var transaction sales_service.CreateTransaction

	err := c.ShouldBindJSON(&transaction)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.TransactionService().Create(c, &transaction)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetTransactionByID godoc
// @ID get_transaction_by_id
// @Router /v1/transaction/{id} [GET]
// @Summary Get Transaction By ID
// @Description Get Transaction By ID
// @Tags Transaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=sales_service.Transaction} "TransactionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTransactionById(c *gin.Context) {

	transactionId := c.Param("id")

	resp, err := h.services.TransactionService().GetByID(c, &sales_service.TransactionPrimaryKey{Id: transactionId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetTransactionList godoc
// @ID get_transaction_list
// @Router /v1/transaction [GET]
// @Summary Get Transaction s List
// @Description  Get Transaction s List
// @Tags Transaction
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchName query string false "searchName"
// @Param searchPhone query string false "searchPhone"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=sales_service.GetListTransactionResponse} "GetAllTransactionResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTransactionList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.TransactionService().GetList(
		context.Background(),
		&sales_service.GetListTransactionRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("name"),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateTransaction godoc
// @ID update_transaction
// @Router /v1/transaction/{id} [PUT]
// @Summary Update Transaction
// @Description Update Transaction
// @Tags Transaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body sales_service.UpdateTransaction true "UpdateTransactionRequestBody"
// @Success 200 {object} http.Response{data=sales_service.Transaction} "Transaction data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateTransaction(c *gin.Context) {
	var transaction sales_service.UpdateTransaction

	transaction.Id = c.Param("id")

	err := c.ShouldBindJSON(&transaction)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.TransactionService().Update(
		c.Request.Context(),
		&transaction,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteTransaction godoc
// @ID delete_transaction
// @Router /v1/transaction/{id} [DELETE]
// @Summary Delete Transaction
// @Description Delete Transaction
// @Tags Transaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Transaction data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteTransaction(c *gin.Context) {

	transactionId := c.Param("id")

	resp, err := h.services.TransactionService().Delete(
		c.Request.Context(),
		&sales_service.TransactionPrimaryKey{Id: transactionId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
