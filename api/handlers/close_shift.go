package handlers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"gitlab.com/market93934/market_go_api_gateway/genproto/sales_service"
)

// @Security ApiKeyAuth
// CloseShift godoc
// @ID close_shift
// @Router /v1/close_shift/{id} [GET]
// @Summary CloseShift
// @Description CloseShift
// @Tags CloseShift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data =string} "Success"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CloseShift(c *gin.Context) {
	shiftId := c.Param("id")
	shiftInfo, err := h.services.ShiftService().GetByID(c, &sales_service.ShiftPrimaryKey{
		Id: shiftId,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "shift get by id",
			"message": err.Error(),
		})
		return
	}
	_, err = h.services.ShiftService().Update(c, &sales_service.UpdateShift{
		Id:            cast.ToString(shiftId),
		BranchId:      cast.ToString(shiftInfo.BranchId),
		ProviderId:    cast.ToString(shiftInfo.ProviderId),
		MarketId:      cast.ToString(shiftInfo.MarketId),
		StaffId:       shiftInfo.StaffId,
		Status:        "Closed",
		TransactionId: cast.ToString(shiftInfo.TransactionId),
	})
	if err != nil {
		fmt.Println("error here !!")
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusBadRequest, map[string]interface{}{
		"status":  "Closed",
		"message": "Shift Was Successfully Closed!!",
	})
	return
}
