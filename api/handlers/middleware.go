package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/pkg/helper"
)

// ResponseModel ...
type ResponseModel struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Error   interface{} `json:"error"`
	Data    interface{} `json:"data"`
}

type HasAccessModel struct {
	Id     string
	RoleId string
}

func (h *Handler) AuthMiddleware() gin.HandlerFunc {

	return func(c *gin.Context) {

		value := c.GetHeader("Authorization")

		info, err := helper.ParseClaims(value, h.cfg.SecretKey)

		if err != nil {
			c.AbortWithError(http.StatusForbidden, err)
			return
		}

		c.Set("Auth", info)
		c.Next()
	}
}
