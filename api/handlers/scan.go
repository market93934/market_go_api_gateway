package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/product_service"
	"gitlab.com/market93934/market_go_api_gateway/genproto/sales_service"
)

// @Security ApiKeyAuth
// MakeScan godoc
// @ID make_scan
// @Router /v1/make_scan/{id} [GET]
// @Summary MakeScan
// @Description MakeScan
// @Tags MakeScan
// @Accept json
// @Produce json
// @Param sale_id path string true "sale_id"
// @Param bar_code path string true "bar_code"
// @Success 200 {object} http.Response{data =string} "Success"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) MakeScan(c *gin.Context) {
	sale_id, err := h.getSaleIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	bar_code, err := h.getBarCodeParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	reminder, err := h.services.ReminderService().GetList(c, &product_service.GetListReminderRequest{
		BarCode: bar_code,
	})

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	if reminder.Count == 0 {
		product, err := h.services.ProductService().GetList(c, &product_service.GetListProductRequest{
			BarCode: bar_code,
		})

		if err != nil {
			c.JSON(http.StatusBadRequest, map[string]interface{}{
				"status":  "GRPC ERROR",
				"message": err.Error(),
			})
			return
		}
		for _, value := range product.Products {
			sale_products, err := h.services.SaleProductService().GetList(c, &sales_service.GetListSalesProductRequest{
				BarCode: bar_code,
			})
			if err != nil {
				c.JSON(http.StatusBadRequest, map[string]interface{}{
					"status":  "GRPC ERROR",
					"message": err.Error(),
				})
				return
			}
			if sale_products.Count == 0 {
				sale, err := h.services.SaleService().GetList(c, &sales_service.GetListSalesRequest{
					Search: sale_id,
				})
				if err != nil {
					c.JSON(http.StatusBadRequest, map[string]interface{}{
						"status":  "GRPC ERROR",
						"message": err.Error(),
					})
					return
				}
				for _, sales := range sale.Saless {
					_, err = h.services.SaleProductService().Create(c, &sales_service.CreateSalesProduct{
						BrandId:     value.BrandId,
						ProductId:   value.Id,
						BarCode:     value.BarCode,
						RemainderId: value.Id,
						Count:       1,
						Price:       value.Price,
						CategoryId:  value.CategotyId,
						SaleId:      sales.Id,
					})
					if err != nil {
						c.JSON(http.StatusBadRequest, map[string]interface{}{
							"status":  "GRPC ERROR",
							"message": err.Error(),
						})
						return
					}
				}
			} else {
				for _, temp := range sale_products.SalesProducts {
					sl_product, err := h.services.SaleProductService().GetByID(c, &sales_service.SalesProductPrimaryKey{
						Id: temp.Id,
					})
					if err != nil {
						c.JSON(http.StatusBadRequest, map[string]interface{}{
							"status":  "GRPC ERROR",
							"message": err.Error(),
						})
						return
					}
					_, err = h.services.SaleProductService().Update(c, &sales_service.UpdateSalesProduct{
						Id:          temp.Id,
						BrandId:     temp.BrandId,
						ProductId:   temp.ProductId,
						BarCode:     temp.BarCode,
						RemainderId: temp.RemainderId,
						Count:       sl_product.Count + 1,
						Price:       temp.Price,
						CategoryId:  temp.CategoryId,
						SaleId:      temp.SaleId,
					})
					if err != nil {
						c.JSON(http.StatusBadRequest, map[string]interface{}{
							"status":  "GRPC ERROR",
							"message": err.Error(),
						})
						return
					}
				}
			}
			if err != nil {
				c.JSON(http.StatusBadRequest, map[string]interface{}{
					"status":  "GRPC ERROR",
					"message": err.Error(),
				})
				return
			}
			sales, err := h.services.SaleService().GetList(c, &sales_service.GetListSalesRequest{
				Search: sale_id,
			})
			if err != nil {
				c.JSON(http.StatusBadRequest, map[string]interface{}{
					"status":  "GRPC ERROR",
					"message": err.Error(),
				})
				return
			}
			for _, val := range sales.Saless {
				h.services.ReminderService().Create(c, &product_service.CreateReminder{
					BranchId:   val.BranchId,
					CategotyId: value.CategotyId,
					BrandId:    value.BrandId,
					ProductId:  value.Id,
					BarCode:    value.BarCode,
					Count:      100,
					Price:      value.Price,
				})
			}
		}

	}
}
