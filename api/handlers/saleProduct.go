package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/sales_service"
)

// @Security ApiKeyAuth
// CreateSalesProduct godoc
// @ID create_sale_product
// @Router /v1/sale_product [POST]
// @Summary  Create SalesProduct
// @Description Create SalesProduct
// @Tags SalesProduct
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body sales_service.CreateSalesProduct true "CreateSalesProductRequestBody"
// @Success 200 {object} http.Response{data=sales_service.SalesProduct} "GetSalesProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSalesProduct(c *gin.Context) {
	var sale_product sales_service.CreateSalesProduct

	err := c.ShouldBindJSON(&sale_product)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.SaleProductService().Create(c, &sale_product)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetSalesProductByID godoc
// @ID get_sale_product_by_id
// @Router /v1/sale_product/{id} [GET]
// @Summary Get SalesProduct By ID
// @Description Get SalesProduct By ID
// @Tags SalesProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=sales_service.SalesProduct} "SalesProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalesProductById(c *gin.Context) {

	sale_productId := c.Param("id")

	resp, err := h.services.SaleProductService().GetByID(c, &sales_service.SalesProductPrimaryKey{Id: sale_productId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetSalesProductList godoc
// @ID get_sale_product_list
// @Router /v1/sale_product [GET]
// @Summary Get SalesProduct s List
// @Description  Get SalesProduct s List
// @Tags SalesProduct
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param brand_id query string false "brand_id"
// @Param bar_code query string false "bar_code"
// @Param category_id query string false "category_id"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=sales_service.GetListSalesProductResponse} "GetAllSalesProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalesProductList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	brand_id, err := h.getBranchIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	bar_code, err := h.getBarCodeParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	category_id, err := h.getCategoryIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.SaleProductService().GetList(
		context.Background(),
		&sales_service.GetListSalesProductRequest{
			Limit:            int64(limit),
			Offset:           int64(offset),
			SearchBrandId:    brand_id,
			BarCode:          bar_code,
			SearchCategoryId: category_id,
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateSalesProduct godoc
// @ID update_sale_product
// @Router /v1/sale_product/{id} [PUT]
// @Summary Update SalesProduct
// @Description Update SalesProduct
// @Tags SalesProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body sales_service.UpdateSalesProduct true "UpdateSalesProductRequestBody"
// @Success 200 {object} http.Response{data=sales_service.SalesProduct} "SalesProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSalesProduct(c *gin.Context) {
	var sale_product sales_service.UpdateSalesProduct

	sale_product.Id = c.Param("id")

	err := c.ShouldBindJSON(&sale_product)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.SaleProductService().Update(
		c.Request.Context(),
		&sale_product,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteSalesProduct godoc
// @ID delete_sale_product
// @Router /v1/sale_product/{id} [DELETE]
// @Summary Delete SalesProduct
// @Description Delete SalesProduct
// @Tags SalesProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "SalesProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSalesProduct(c *gin.Context) {

	sale_productId := c.Param("id")

	resp, err := h.services.SaleProductService().Delete(
		c.Request.Context(),
		&sales_service.SalesProductPrimaryKey{Id: sale_productId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
