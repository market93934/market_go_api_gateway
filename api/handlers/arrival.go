package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/arrival_service"
)

// @Security ApiKeyAuth
// CreateArrival godoc
// @ID create_arrival
// @Router /v1/arrival [POST]
// @Summary  Create Arrival
// @Description Create Arrival
// @Tags Arrival
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body arrival_service.CreateArrival true "CreateArrivalRequestBody"
// @Success 200 {object} http.Response{data=arrival_service.Arrival} "GetArrivalBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateArrival(c *gin.Context) {
	var (
		arrival arrival_service.CreateArrival
	)

	err := c.ShouldBindJSON(&arrival)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ArrivalService().Create(c, &arrival)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetArrivalByID godoc
// @ID get_arrival_by_id
// @Router /v1/arrival/{id} [GET]
// @Summary Get Arrival By ID
// @Description Get Arrival By ID
// @Tags Arrival
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=arrival_service.Arrival} "ArrivalBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetArrivalById(c *gin.Context) {

	arrivalId := c.Param("id")

	resp, err := h.services.ArrivalService().GetByID(c, &arrival_service.ArrivalPrimaryKey{Id: arrivalId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetArrivalList godoc
// @ID get_arrival_list
// @Router /v1/arrival [GET]
// @Summary Get Arrival s List
// @Description  Get Arrival s List
// @Tags Arrival
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param arrival_id query string false "arrival_id"
// @Param brand_id query string false "brand_id"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=arrival_service.GetListArrivalResponse} "GetAllArrivalResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetArrivalList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	arrivalId, err := h.getArrivalIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	brandId, err := h.getBrandIdParam(c)
	resp, err := h.services.ArrivalService().GetList(
		context.Background(),
		&arrival_service.GetListArrivalRequest{
			Limit:           int64(limit),
			Offset:          int64(offset),
			SearchArrivalId: arrivalId,
			SearchBranchId:  brandId,
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateArrival godoc
// @ID update_arrival
// @Router /v1/arrival/{id} [PUT]
// @Summary Update Arrival
// @Description Update Arrival
// @Tags Arrival
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body arrival_service.UpdateArrival true "UpdateArrivalRequestBody"
// @Success 200 {object} http.Response{data=arrival_service.Arrival} "Arrival data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateArrival(c *gin.Context) {
	var arrival arrival_service.UpdateArrival

	arrival.Id = c.Param("id")

	err := c.ShouldBindJSON(&arrival)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ArrivalService().Update(
		c.Request.Context(),
		&arrival,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteArrival godoc
// @ID delete_arrival
// @Router /v1/arrival/{id} [DELETE]
// @Summary Delete Arrival
// @Description Delete Arrival
// @Tags Arrival
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Arrival data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteArrival(c *gin.Context) {

	arrivalId := c.Param("id")

	resp, err := h.services.ArrivalService().Delete(
		c.Request.Context(),
		&arrival_service.ArrivalPrimaryKey{Id: arrivalId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
