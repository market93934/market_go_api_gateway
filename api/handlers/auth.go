package handlers

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/market93934/market_go_api_gateway/api/models"
	"gitlab.com/market93934/market_go_api_gateway/genproto/user_service"
	"gitlab.com/market93934/market_go_api_gateway/pkg/helper"
)

// Login godoc
// @ID login
// @Router /login [POST]
// @Summary Login
// @Description Login
// @Tags Login
// @Accept json
// @Procedure json
// @Param login body models.LoginInfo true "LoginRequest"
// @Success 200 {object} http.Response{data=string} "Success Request"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server error"
func (h *Handler) Login(c *gin.Context) {
	var login models.LoginInfo

	err := c.ShouldBindJSON(&login) // parse req body to given type struct
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.StaffService().GetByID(context.Background(), &user_service.StaffPrimaryKey{Login: login.Login})
	if err != nil {
		if err.Error() == "no rows in result set" {
			c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"status":  "error",
				"message": "User Does not exist",
			})
			return
		}
		fmt.Println(resp)
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	if login.UserType != resp.UserType {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": "wrong user types!!!",
		})
		return
	}
	match := CheckPasswordHash(login.Password, resp.Password)
	if match == false {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": "password does not match",
		})
		return
	}

	token, err := helper.GenerateJWT(map[string]interface{}{
		"user_id": resp.Id,
	}, time.Hour*360, h.cfg.SecretKey)
	if match == false {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	c.SetCookie("token", token, 60*60*24, "/", "localhost", false, true)
	c.JSON(http.StatusOK, map[string]interface{}{
		"status":  "OK",
		"message": "Loged in successfully",
	})
}

// Register godoc
// @ID register
// @Router /register [POST]
// @Summary Register
// @Description Register
// @Tags Register
// @Accept json
// @Procedure json
// @Param register body user_service.CreateStaff true "CreateUserRequest"
// @Success 200 {object} http.Response{data=string} "Success Request"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server error"
func (h *Handler) Register(c *gin.Context) {

	var createUser user_service.CreateStaff
	err := c.ShouldBindJSON(&createUser)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": "shouldBindJSON",
		})
		return
	}
	h.services.StaffService().Create(c, &createUser)
	if len(createUser.Password) < 7 {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": "password has to contain more than 7 symbols!!",
		})
		return
	}
	fmt.Println(createUser.Login)
	resp, err := h.services.StaffService().GetByID(context.Background(), &user_service.StaffPrimaryKey{Login: createUser.Login})
	if err != nil {
		if err.Error() == "no rows in result set" {
			resp, err = h.services.StaffService().Create(c, &createUser)
			if err != nil {
				c.JSON(http.StatusInternalServerError, map[string]interface{}{
					"status":  "error",
					"message": err.Error(),
				})
				return
			}
		} else {
			c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"status":  "error",
				"message": "User Already exists, please log in",
			})
			return
		}
	} 
	resp, err = h.services.StaffService().GetByID(context.Background(), &user_service.StaffPrimaryKey{Id: resp.Id})

	c.JSON(http.StatusInternalServerError, map[string]interface{}{
		"status":  "Created",
		"message": "Successfully Registered",
	})
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
