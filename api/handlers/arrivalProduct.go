package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/arrival_service"
)

// @Security ApiKeyAuth
// CreateArrivalProduct godoc
// @ID create_arrival_product
// @Router /v1/arrival_product [POST]
// @Summary  Create ArrivalProduct
// @Description Create ArrivalProduct
// @Tags ArrivalProduct
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body arrival_service.CreateArrivalProduct true "CreateArrivalProductRequestBody"
// @Success 200 {object} http.Response{data=arrival_service.ArrivalProduct} "GetArrivalProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateArrivalProduct(c *gin.Context) {
	var arrival_product arrival_service.CreateArrivalProduct

	err := c.ShouldBindJSON(&arrival_product)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ArrivalProductService().Create(c, &arrival_product)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetArrivalProductByID godoc
// @ID get_arrival_product_by_id
// @Router /v1/arrival_product/{id} [GET]
// @Summary Get ArrivalProduct By ID
// @Description Get ArrivalProduct By ID
// @Tags ArrivalProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=arrival_service.ArrivalProduct} "ArrivalProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetArrivalProductById(c *gin.Context) {

	arrival_productId := c.Param("id")

	resp, err := h.services.ArrivalProductService().GetByID(c, &arrival_service.ArrivalProductPrimaryKey{Id: arrival_productId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetArrivalProductList godoc
// @ID get_arrival_product_list
// @Router /v1/arrival_product [GET]
// @Summary Get ArrivalProduct s List
// @Description  Get ArrivalProduct s List
// @Tags ArrivalProduct
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param brand_id query string false "brand_id"
// @Param ctegory_id query string false "ctegory_id"
// @Param bar_code query string false "bar_code"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=arrival_service.GetListArrivalProductResponse} "GetAllArrivalProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetArrivalProductList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	brandId, err := h.getBrandIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	categoryId, err := h.getCategoryIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	barCodeId, err := h.getBarCodeParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ArrivalProductService().GetList(
		context.Background(),
		&arrival_service.GetListArrivalProductRequest{
			Limit:            int64(limit),
			Offset:           int64(offset),
			SearchBrandId:    brandId,
			SearchCategoryId: categoryId,
			SearchBarCode:    barCodeId,
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateArrivalProduct godoc
// @ID update_arrival_product
// @Router /v1/arrival_product/{id} [PUT]
// @Summary Update ArrivalProduct
// @Description Update ArrivalProduct
// @Tags ArrivalProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body arrival_service.UpdateArrivalProduct true "UpdateArrivalProductRequestBody"
// @Success 200 {object} http.Response{data=arrival_service.ArrivalProduct} "ArrivalProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateArrivalProduct(c *gin.Context) {
	var arrival_product arrival_service.UpdateArrivalProduct

	arrival_product.Id = c.Param("id")

	err := c.ShouldBindJSON(&arrival_product)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ArrivalProductService().Update(
		c.Request.Context(),
		&arrival_product,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteArrivalProduct godoc
// @ID delete_arrival_product
// @Router /v1/arrival_product/{id} [DELETE]
// @Summary Delete ArrivalProduct
// @Description Delete ArrivalProduct
// @Tags ArrivalProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "ArrivalProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteArrivalProduct(c *gin.Context) {

	arrival_productId := c.Param("id")

	resp, err := h.services.ArrivalProductService().Delete(
		c.Request.Context(),
		&arrival_service.ArrivalProductPrimaryKey{Id: arrival_productId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
