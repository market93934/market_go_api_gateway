package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/product_service"
)

// @Security ApiKeyAuth
// CreateProduct godoc
// @ID create_product
// @Router /v1/product [POST]
// @Summary  Create Product
// @Description Create Product
// @Tags Product
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body product_service.CreateProduct true "CreateProductRequestBody"
// @Success 200 {object} http.Response{data=product_service.Product} "GetProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateProduct(c *gin.Context) {
	var product product_service.CreateProduct

	err := c.ShouldBindJSON(&product)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ProductService().Create(c, &product)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetProductByID godoc
// @ID get_product_by_id
// @Router /v1/product/{id} [GET]
// @Summary Get Product By ID
// @Description Get Product By ID
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=product_service.Product} "ProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetProductById(c *gin.Context) {

	productId := c.Param("id")

	resp, err := h.services.ProductService().GetByID(c, &product_service.ProductPrimaryKey{Id: productId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetProductList godoc
// @ID get_product_list
// @Router /v1/product [GET]
// @Summary Get Product s List
// @Description  Get Product s List
// @Tags Product
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param name query string false "name"
// @Param bar_code query string false "bar_code"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=product_service.GetListProductResponse} "GetAllProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetProductList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	name, err := h.getDefaultNameParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	bar_code, err := h.getBarCodeParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ProductService().GetList(
		context.Background(),
		&product_service.GetListProductRequest{
			Limit:      int64(limit),
			Offset:     int64(offset),
			SearchName: name,
			BarCode:    bar_code,
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateProduct godoc
// @ID update_product
// @Router /v1/product/{id} [PUT]
// @Summary Update Product
// @Description Update Product
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body product_service.UpdateProduct true "UpdateProductRequestBody"
// @Success 200 {object} http.Response{data=product_service.Product} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateProduct(c *gin.Context) {
	var product product_service.UpdateProduct

	product.Id = c.Param("id")

	err := c.ShouldBindJSON(&product)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ProductService().Update(
		c.Request.Context(),
		&product,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteProduct godoc
// @ID delete_product
// @Router /v1/product/{id} [DELETE]
// @Summary Delete Product
// @Description Delete Product
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteProduct(c *gin.Context) {

	productId := c.Param("id")

	resp, err := h.services.ProductService().Delete(
		c.Request.Context(),
		&product_service.ProductPrimaryKey{Id: productId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
