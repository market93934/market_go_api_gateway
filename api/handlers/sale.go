package handlers

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/sales_service"
	"gitlab.com/market93934/market_go_api_gateway/genproto/user_service"
	"gitlab.com/market93934/market_go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// CreateSales godoc
// @ID create_sale
// @Router /v1/sale [POST]
// @Summary  Create Sales
// @Description Create Sales
// @Tags Sales
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body sales_service.CreateSales true "CreateSalesRequestBody"
// @Success 200 {object} http.Response{data=sales_service.Sales} "GetSalesBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSales(c *gin.Context) {

	var (
		sale sales_service.CreateSales
	)

	err := c.ShouldBindJSON(&sale)
	if err != nil {
		fmt.Println("error")
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.SaleService().Create(c, &sale)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	branch, err := h.services.BranchService().GetByID(c, &user_service.BranchPrimaryKey{
		Id: resp.BranchId,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	sales, err := h.services.SaleService().GetList(c, &sales_service.GetListSalesRequest{})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	sale_id := helper.GenerateString(string(branch.Name[0]), int(sales.Count))
	_, err = h.services.SaleService().Update(c, &sales_service.UpdateSales{
		Id:        resp.Id,
		BranchId:  resp.BranchId,
		ShiftId:   resp.ShiftId,
		MarketId:  resp.MarketId,
		StaffId:   resp.StaffId,
		Status:    resp.Status,
		PaymentId: resp.PaymentId,
		SaleId:    sale_id,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetSalesByID godoc
// @ID get_sale_by_id
// @Router /v1/sale/{id} [GET]
// @Summary Get Sales By ID
// @Description Get Sales By ID
// @Tags Sales
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=sales_service.Sales} "SalesBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalesById(c *gin.Context) {

	saleId := c.Param("id")

	resp, err := h.services.SaleService().GetByID(c, &sales_service.SalesPrimaryKey{Id: saleId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetSalesList godoc
// @ID get_sale_list
// @Router /v1/sale [GET]
// @Summary Get Sales s List
// @Description  Get Sales s List
// @Tags Sales
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param sale_id query string false "sale_id"
// @Param shift_id query string false "shift_id"
// @Param branch_id query string false "branch_id"
// @Param staff_id query string false "staff_id"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=sales_service.GetListSalesResponse} "GetAllSalesResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalesList(c *gin.Context) {

	sale_id, err := h.getSaleIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	shift_id, err := h.getShiftIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	branch_id, err := h.getBranchIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	staff_id, err := h.getStaffIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.SaleService().GetList(
		context.Background(),
		&sales_service.GetListSalesRequest{
			Limit:          int64(limit),
			Offset:         int64(offset),
			Search:         sale_id,
			SearchShiftId:  shift_id,
			SearchBranchId: branch_id,
			SearchStaffId:  staff_id,
		},
	)
	if err != nil {
		fmt.Println("eroor")
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateSales godoc
// @ID update_sale
// @Router /v1/sale/{id} [PUT]
// @Summary Update Sales
// @Description Update Sales
// @Tags Sales
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body sales_service.UpdateSales true "UpdateSalesRequestBody"
// @Success 200 {object} http.Response{data=sales_service.Sales} "Sales data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSales(c *gin.Context) {
	var sale sales_service.UpdateSales

	sale.Id = c.Param("id")

	err := c.ShouldBindJSON(&sale)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.SaleService().Update(
		c.Request.Context(),
		&sale,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteSales godoc
// @ID delete_sale
// @Router /v1/sale/{id} [DELETE]
// @Summary Delete Sales
// @Description Delete Sales
// @Tags Sales
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Sales data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSales(c *gin.Context) {

	saleId := c.Param("id")

	resp, err := h.services.SaleService().Delete(
		c.Request.Context(),
		&sales_service.SalesPrimaryKey{Id: saleId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
