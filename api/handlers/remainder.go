package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/product_service"
)

// @Security ApiKeyAuth
// CreateReminder godoc
// @ID create_reminder
// @Router /v1/reminder [POST]
// @Summary  Create Reminder
// @Description Create Reminder
// @Tags Reminder
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body product_service.CreateReminder true "CreateReminderRequestBody"
// @Success 200 {object} http.Response{data=product_service.Reminder} "GetReminderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateReminder(c *gin.Context) {
	var reminder product_service.CreateReminder

	err := c.ShouldBindJSON(&reminder)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ReminderService().Create(c, &reminder)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetReminderByID godoc
// @ID get_reminder_by_id
// @Router /v1/reminder/{id} [GET]
// @Summary Get Reminder By ID
// @Description Get Reminder By ID
// @Tags Reminder
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=product_service.Reminder} "ReminderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetReminderById(c *gin.Context) {

	reminderId := c.Param("id")

	resp, err := h.services.ReminderService().GetByID(c, &product_service.ReminderPrimaryKey{ProductId: reminderId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetReminderList godoc
// @ID get_reminder_list
// @Router /v1/reminder [GET]
// @Summary Get Reminder s List
// @Description  Get Reminder s List
// @Tags Reminder
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param brand_id query string false "brand_id"
// @Param branch_id query string false "branch_id"
// @Param bar_code query string false "bar_code"
// @Param category_id query string false "category_id"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=product_service.GetListReminderResponse} "GetAllReminderResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetReminderList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	branch_id, err := h.getBranchIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	brand_id, err := h.getBrandIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	bar_code, err := h.getBarCodeParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	category_id, err := h.getCategoryIdParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ReminderService().GetList(
		context.Background(),
		&product_service.GetListReminderRequest{
			Limit:          int64(limit),
			Offset:         int64(offset),
			SearchBrandId:  brand_id,
			BranchId:       branch_id,
			BarCode:        bar_code,
			SearchCategory: category_id,
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateReminder godoc
// @ID update_reminder
// @Router /v1/reminder/{id} [PUT]
// @Summary Update Reminder
// @Description Update Reminder
// @Tags Reminder
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body product_service.UpdateReminder true "UpdateReminderRequestBody"
// @Success 200 {object} http.Response{data=product_service.Reminder} "Reminder data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateReminder(c *gin.Context) {
	var reminder product_service.UpdateReminder

	reminder.ProductId = c.Param("id")

	err := c.ShouldBindJSON(&reminder)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ReminderService().Update(
		c.Request.Context(),
		&reminder,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteReminder godoc
// @ID delete_reminder
// @Router /v1/reminder/{id} [DELETE]
// @Summary Delete Reminder
// @Description Delete Reminder
// @Tags Reminder
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Reminder data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteReminder(c *gin.Context) {

	reminderId := c.Param("id")

	resp, err := h.services.ReminderService().Delete(
		c.Request.Context(),
		&product_service.ReminderPrimaryKey{ProductId: reminderId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
