package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/sales_service"
)

// @Security ApiKeyAuth
// OpenShift godoc
// @ID open_shift
// @Router /v1/open_shift [POST]
// @Summary  OpenShift
// @Description OpenShift
// @Tags OpenShift
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body sales_service.CreateShift true "CreateShiftRequestBody"
// @Success 200 {object} http.Response{data=sales_service.Shift} "GetShiftBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) OpenShiftShift(c *gin.Context) {
	var shift sales_service.CreateShift

	err := c.ShouldBindJSON(&shift)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ShiftService().Create(c, &shift)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, resp)

}
