package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market93934/market_go_api_gateway/genproto/user_service"
)

// @Security ApiKeyAuth
// CreateMarket godoc
// @ID create_market
// @Router /v1/market [POST]
// @Summary  Create Market
// @Description Create Market
// @Tags Market
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body user_service.CreateMarket true "CreateMarketRequestBody"
// @Success 200 {object} http.Response{data=user_service.Market} "GetMarketBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateMarket(c *gin.Context) {
	var market user_service.CreateMarket

	err := c.ShouldBindJSON(&market)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.MarketService().Create(c, &market)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetMarketByID godoc
// @ID get_market_by_id
// @Router /v1/market/{id} [GET]
// @Summary Get Market By ID
// @Description Get Market By ID
// @Tags Market
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.Market} "MarketBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetMarketById(c *gin.Context) {

	marketId := c.Param("id")

	resp, err := h.services.MarketService().GetByID(c, &user_service.MarketPrimaryKey{Id: marketId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetMarketList godoc
// @ID get_market_list
// @Router /v1/market [GET]
// @Summary Get Market s List
// @Description  Get Market s List
// @Tags Market
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param name query string false "name"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=user_service.GetListMarketResponse} "GetAllMarketResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetMarketList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	name, err := h.getDefaultNameParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.MarketService().GetList(
		context.Background(),
		&user_service.GetListMarketRequest{
			Limit:      int64(limit),
			Offset:     int64(offset),
			SearchName: name,
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateMarket godoc
// @ID update_market
// @Router /v1/market/{id} [PUT]
// @Summary Update Market
// @Description Update Market
// @Tags Market
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateMarket true "UpdateMarketRequestBody"
// @Success 200 {object} http.Response{data=user_service.Market} "Market data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateMarket(c *gin.Context) {
	var market user_service.UpdateMarket

	market.Id = c.Param("id")

	err := c.ShouldBindJSON(&market)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.MarketService().Update(
		c.Request.Context(),
		&market,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteMarket godoc
// @ID delete_market
// @Router /v1/market/{id} [DELETE]
// @Summary Delete Market
// @Description Delete Market
// @Tags Market
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Market data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteMarket(c *gin.Context) {

	marketId := c.Param("id")

	resp, err := h.services.MarketService().Delete(
		c.Request.Context(),
		&user_service.MarketPrimaryKey{Id: marketId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
