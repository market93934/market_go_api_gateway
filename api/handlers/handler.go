package handlers

import (
	"encoding/json"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/gogo/protobuf/jsonpb"
	"gitlab.com/market93934/market_go_api_gateway/api/http"
	"gitlab.com/market93934/market_go_api_gateway/config"
	"gitlab.com/market93934/market_go_api_gateway/pkg/logger"
	"gitlab.com/market93934/market_go_api_gateway/services"
	"google.golang.org/protobuf/runtime/protoiface"
)

type Handler struct {
	cfg      config.Config
	log      logger.LoggerI
	services services.ServiceManagerI
}

func NewHandler(cfg config.Config, log logger.LoggerI, srvc services.ServiceManagerI) *Handler {
	return &Handler{
		cfg:      cfg,
		log:      log,
		services: srvc,
	}
}
func (h *Handler) handleResponse(c *gin.Context, status http.Status, data interface{}) {
	switch code := status.Code; {
	case code < 300:
		h.log.Info(
			"response",
			logger.Int("code", status.Code),
			logger.String("status", status.Status),
			logger.Any("description", status.Description),
			// logger.Any("data", data),
		)
	case code < 400:
		h.log.Warn(
			"response",
			logger.Int("code", status.Code),
			logger.String("status", status.Status),
			logger.Any("description", status.Description),
			logger.Any("data", data),
		)
	default:
		h.log.Error(
			"response",
			logger.Int("code", status.Code),
			logger.String("status", status.Status),
			logger.Any("description", status.Description),
			logger.Any("data", data),
		)
	}

	c.JSON(status.Code, data)
}

func ProtoToStruct(s interface{}, p protoiface.MessageV1) error {
	var jm jsonpb.Marshaler

	jm.EmitDefaults = true
	jm.OrigName = true

	ms, err := jm.MarshalToString(p)

	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(ms), &s)

	return err
}
func (h *Handler) getLastNameParam(c *gin.Context) (last_name string, err error) {
	if h.cfg.DefaultBrandId != "" {
		last_name := c.DefaultQuery("last_name", h.cfg.DefaultBarCode)
		return last_name, nil
	}
	last_nameStr := c.DefaultQuery("last_name", "")
	return last_nameStr, nil
}
func (h *Handler) getPhoneNumberParam(c *gin.Context) (phone_number string, err error) {
	if h.cfg.DefaultBrandId != "" {
		phone_number := c.DefaultQuery("phone_number", h.cfg.DefaultBarCode)
		return phone_number, nil
	}
	phone_numberStr := c.DefaultQuery("phone_number", "")
	return phone_numberStr, nil
}
func (h *Handler) getmarketIdParam(c *gin.Context) (market_id string, err error) {
	if h.cfg.DefaultBrandId != "" {
		market_id := c.DefaultQuery("market_id", h.cfg.DefaultBarCode)
		return market_id, nil
	}
	market_idStr := c.DefaultQuery("market_id", "")
	return market_idStr, nil
}
func (h *Handler) getStaffIdParam(c *gin.Context) (shift_id string, err error) {
	if h.cfg.DefaultBrandId != "" {
		staff_id := c.DefaultQuery("staff_id", h.cfg.DefaultBarCode)
		return staff_id, nil
	}
	shift_idStr := c.DefaultQuery("brand_category_idid", "")
	return shift_idStr, nil
}
func (h *Handler) getShiftIdParam(c *gin.Context) (shift_id string, err error) {
	if h.cfg.DefaultBrandId != "" {
		shift_id := c.DefaultQuery("shift_id", h.cfg.DefaultBarCode)
		return shift_id, nil
	}
	shift_idStr := c.DefaultQuery("brand_category_idid", "")
	return shift_idStr, nil
}
func (h *Handler) getCategoryIdParam(c *gin.Context) (category_id string, err error) {
	if h.cfg.DefaultBrandId != "" {
		category_id := c.DefaultQuery("category_id", h.cfg.DefaultBarCode)
		return category_id, nil
	}
	category_idStr := c.DefaultQuery("brand_category_id", "")
	return category_idStr, nil
}
func (h *Handler) getBrandIdParam(c *gin.Context) (brandId string, err error) {
	if h.cfg.DefaultBrandId != "" {
		brand_id := c.DefaultQuery("brand_id", h.cfg.DefaultBarCode)
		return brand_id, nil
	}
	brand_id := c.DefaultQuery("brand_id", "")
	return brand_id, nil
}
func (h *Handler) getDefaultNameParam(c *gin.Context) (name string, err error) {
	if h.cfg.DefaultBranchId != "" {
		nme := c.DefaultQuery("name", h.cfg.DefaultBarCode)
		return nme, nil
	}
	nme := c.DefaultQuery("name", "")
	return nme, nil
}
func (h *Handler) getBranchIdParam(c *gin.Context) (branchId string, err error) {
	if h.cfg.DefaultBranchId != "" {
		branch_id := c.DefaultQuery("branch_id", h.cfg.DefaultBarCode)
		return branch_id, nil
	}
	branch_id := c.DefaultQuery("branch_id", "")
	return branch_id, nil
}
func (h *Handler) getArrivalIdParam(c *gin.Context) (arrivalId string, err error) {
	if h.cfg.DefaultArrivalId != "" {
		arrival_id := c.DefaultQuery("arrival_id", h.cfg.DefaultBarCode)
		return arrival_id, nil
	}
	arrival_id := c.DefaultQuery("arrival_id", "")
	return arrival_id, nil
}
func (h *Handler) getBarCodeParam(c *gin.Context) (barCode string, err error) {
	if h.cfg.DefaultBarCode != "" {
		bar_code := c.DefaultQuery("bar_code", h.cfg.DefaultBarCode)
		return bar_code, nil
	}
	bar_code := c.DefaultQuery("bar_code", "")
	return bar_code, nil
}
func (h *Handler) getSaleIdParam(c *gin.Context) (saleId string, err error) {
	if h.cfg.DefaultSaleId != "" {
		sale_id := c.DefaultQuery("sale_id", h.cfg.DefaultSaleId)
		return sale_id, nil
	}
	sale_id := c.DefaultQuery("sale_id", "")
	return sale_id, nil
}
func (h *Handler) getOffsetParam(c *gin.Context) (offset int, err error) {
	if h.cfg.DefaultOffset != "" {
		offsetStr := c.DefaultQuery("offset", h.cfg.DefaultOffset)
		return strconv.Atoi(offsetStr)
	}

	offsetStr := c.DefaultQuery("offset", "0")
	return strconv.Atoi(offsetStr)
}

func (h *Handler) getLimitParam(c *gin.Context) (offset int, err error) {
	if h.cfg.DefaultLimit != "" {
		limitStr := c.DefaultQuery("limit", h.cfg.DefaultLimit)
		return strconv.Atoi(limitStr)
	}
	limitStr := c.DefaultQuery("limit", "10")
	return strconv.Atoi(limitStr)
}
func (h *Handler) getFromParam(c *gin.Context) (from string, err error) {
	if h.cfg.DefaultFrom != "" {
		fromStr := c.DefaultQuery("from", h.cfg.DefaultFrom)
		return fromStr, nil
	}
	fromStr := c.DefaultQuery("from", "01-01-2023")
	return fromStr, nil
}
func (h *Handler) getToParam(c *gin.Context) (to string, err error) {
	if h.cfg.DefaultTo != "" {
		toStr := c.DefaultQuery("to", h.cfg.DefaultFrom)
		return toStr, nil
	}
	toStr := c.DefaultQuery("from", "31-01-2023")
	return toStr, nil
}
func (h *Handler) handleErrorResponse(c *gin.Context, code int, message string, err interface{}) {
	h.log.Error(message, logger.Int("code", code), logger.Any("error", err))
	c.JSON(code, ResponseModel{
		Code:    code,
		Message: message,
		Error:   err,
	})
}

func (h *Handler) handleSuccessResponse(c *gin.Context, code int, message string, data interface{}) {
	c.JSON(code, ResponseModel{
		Code:    code,
		Message: message,
		Data:    data,
	})
}
