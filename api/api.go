package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/market93934/market_go_api_gateway/api/docs"
	handlers "gitlab.com/market93934/market_go_api_gateway/api/handlers"
	"gitlab.com/market93934/market_go_api_gateway/config"
)

func SetUpAPI(r *gin.Engine, h *handlers.Handler, cfg config.Config) {

	// @securityDefinitions.apikey ApiKeyAuth
	// @in header
	// @name Authorization
	
	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(5000))

	v1 := r.Group("/v1")

	r.POST("/login", h.Login)

	// Register Api
	r.POST("/register", h.Register)

	v1.Use(h.AuthMiddleware())
	v1.POST("staff", h.CreateStaff)
	v1.GET("staff/:id", h.GetStaffById)
	v1.GET("staff", h.GetStaffList)
	v1.PUT("staff/:id", h.UpdateStaff)
	v1.DELETE("staff/:id", h.DeleteStaff)

	v1.POST("branch", h.CreateBranch)
	v1.GET("branch/:id", h.GetBranchById)
	v1.GET("branch", h.GetBranchList)
	v1.PUT("branch/:id", h.UpdateBranch)
	v1.DELETE("branch/:id", h.DeleteBranch)

	v1.POST("market", h.CreateMarket)
	v1.GET("market/:id", h.GetMarketById)
	v1.GET("market", h.GetMarketList)
	v1.PUT("market/:id", h.UpdateMarket)
	v1.DELETE("market/:id", h.DeleteMarket)

	v1.POST("provider", h.CreateProvider)
	v1.GET("provider/:id", h.GetProviderById)
	v1.GET("provider", h.GetProviderList)
	v1.PUT("provider/:id", h.UpdateProvider)
	v1.DELETE("provider/:id", h.DeleteProvider)

	v1.POST("product", h.CreateProduct)
	v1.GET("product/:id", h.GetProductById)
	v1.GET("product", h.GetProductList)
	v1.PUT("product/:id", h.UpdateProduct)
	v1.DELETE("product/:id", h.DeleteProduct)

	v1.POST("reminder", h.CreateReminder)
	v1.GET("reminder/:id", h.GetReminderById)
	v1.GET("reminder", h.GetReminderList)
	v1.PUT("reminder/:id", h.UpdateReminder)
	v1.DELETE("reminder/:id", h.DeleteReminder)

	v1.POST("category", h.CreateCategory)
	v1.GET("category/:id", h.GetCategoryById)
	v1.GET("category", h.GetCategoryList)
	v1.PUT("category/:id", h.UpdateCategory)
	v1.DELETE("category/:id", h.DeleteCategory)

	v1.POST("brand", h.CreateBrand)
	v1.GET("brand/:id", h.GetBrandById)
	v1.GET("brand", h.GetBrandList)
	v1.PUT("brand/:id", h.UpdateBrand)
	v1.DELETE("brand/:id", h.DeleteBrand)

	v1.POST("arrival", h.CreateArrival)
	v1.GET("arrival/:id", h.GetArrivalById)
	v1.GET("arrival", h.GetArrivalList)
	v1.PUT("arrival/:id", h.UpdateArrival)
	v1.DELETE("arrival/:id", h.DeleteArrival)

	v1.POST("arrival_product", h.CreateArrivalProduct)
	v1.GET("arrival_product/:id", h.GetArrivalProductById)
	v1.GET("arrival_product", h.GetArrivalProductList)
	v1.PUT("arrival_product/:id", h.UpdateArrivalProduct)
	v1.DELETE("arrival_product/:id", h.DeleteArrivalProduct)

	v1.POST("sale", h.CreateSales)
	v1.GET("sale/:id", h.GetSalesById)
	v1.GET("sale", h.GetSalesList)
	v1.PUT("sale/:id", h.UpdateSales)
	v1.DELETE("sale/:id", h.DeleteSales)

	v1.POST("sale_product", h.CreateSalesProduct)
	v1.GET("sale_product/:id", h.GetSalesProductById)
	v1.GET("sale_product", h.GetSalesProductList)
	v1.PUT("sale_product/:id", h.UpdateSalesProduct)
	v1.DELETE("sale_product/:id", h.DeleteSalesProduct)

	v1.POST("shift", h.CreateShift)
	v1.GET("shift/:id", h.GetShiftById)
	v1.GET("shift", h.GetShiftList)
	v1.PUT("shift/:id", h.UpdateShift)
	v1.DELETE("shift/:id", h.DeleteShift)

	v1.POST("transaction", h.CreateTransaction)
	v1.GET("transaction/:id", h.GetTransactionById)
	v1.GET("transaction", h.GetTransactionList)
	v1.PUT("transaction/:id", h.UpdateTransaction)
	v1.DELETE("transaction/:id", h.DeleteTransaction)

	v1.POST("payment", h.CreatePayment)
	v1.GET("payment/:id", h.GetPaymentById)
	v1.GET("payment", h.GetPaymentList)
	v1.PUT("payment/:id", h.UpdatePayment)
	v1.DELETE("payment/:id", h.DeletePayment)

	v1.GET("make_sale/:id", h.MakeSale)

	v1.GET("make_arrival/:id", h.MakeArrival)

	v1.GET("close_shift/:id", h.CloseShift)

	v1.POST("open_shift", h.OpenShiftShift)

	v1.GET("make_scan/:id", h.MakeScan)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
