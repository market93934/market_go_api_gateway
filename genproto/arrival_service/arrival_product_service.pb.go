// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.21.12
// source: arrival_product_service.proto

package arrival_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type ArrivalProductEmpty struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *ArrivalProductEmpty) Reset() {
	*x = ArrivalProductEmpty{}
	if protoimpl.UnsafeEnabled {
		mi := &file_arrival_product_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ArrivalProductEmpty) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ArrivalProductEmpty) ProtoMessage() {}

func (x *ArrivalProductEmpty) ProtoReflect() protoreflect.Message {
	mi := &file_arrival_product_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ArrivalProductEmpty.ProtoReflect.Descriptor instead.
func (*ArrivalProductEmpty) Descriptor() ([]byte, []int) {
	return file_arrival_product_service_proto_rawDescGZIP(), []int{0}
}

var File_arrival_product_service_proto protoreflect.FileDescriptor

var file_arrival_product_service_proto_rawDesc = []byte{
	0x0a, 0x1d, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63,
	0x74, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x0f, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x1a, 0x15, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63,
	0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x15, 0x0a, 0x13, 0x41, 0x72, 0x72, 0x69, 0x76,
	0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x32, 0xe1,
	0x03, 0x0a, 0x15, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63,
	0x74, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x52, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61,
	0x74, 0x65, 0x12, 0x25, 0x2e, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x41, 0x72, 0x72, 0x69, 0x76,
	0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x1a, 0x1f, 0x2e, 0x61, 0x72, 0x72, 0x69,
	0x76, 0x61, 0x6c, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x41, 0x72, 0x72, 0x69,
	0x76, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x22, 0x00, 0x12, 0x57, 0x0a, 0x07,
	0x47, 0x65, 0x74, 0x42, 0x79, 0x49, 0x44, 0x12, 0x29, 0x2e, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61,
	0x6c, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61,
	0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b,
	0x65, 0x79, 0x1a, 0x1f, 0x2e, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64,
	0x75, 0x63, 0x74, 0x22, 0x00, 0x12, 0x6a, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74,
	0x12, 0x2d, 0x2e, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x2e, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61,
	0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a,
	0x2e, 0x2e, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x2e, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c,
	0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22,
	0x00, 0x12, 0x52, 0x0a, 0x06, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x25, 0x2e, 0x61, 0x72,
	0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x55, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75,
	0x63, 0x74, 0x1a, 0x1f, 0x2e, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64,
	0x75, 0x63, 0x74, 0x22, 0x00, 0x12, 0x5b, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x12,
	0x29, 0x2e, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x2e, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74,
	0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x1a, 0x24, 0x2e, 0x61, 0x72, 0x72,
	0x69, 0x76, 0x61, 0x6c, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x41, 0x72, 0x72,
	0x69, 0x76, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x45, 0x6d, 0x70, 0x74, 0x79,
	0x22, 0x00, 0x42, 0x1a, 0x5a, 0x18, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x61,
	0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_arrival_product_service_proto_rawDescOnce sync.Once
	file_arrival_product_service_proto_rawDescData = file_arrival_product_service_proto_rawDesc
)

func file_arrival_product_service_proto_rawDescGZIP() []byte {
	file_arrival_product_service_proto_rawDescOnce.Do(func() {
		file_arrival_product_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_arrival_product_service_proto_rawDescData)
	})
	return file_arrival_product_service_proto_rawDescData
}

var file_arrival_product_service_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_arrival_product_service_proto_goTypes = []interface{}{
	(*ArrivalProductEmpty)(nil),           // 0: arrival_service.ArrivalProductEmpty
	(*CreateArrivalProduct)(nil),          // 1: arrival_service.CreateArrivalProduct
	(*ArrivalProductPrimaryKey)(nil),      // 2: arrival_service.ArrivalProductPrimaryKey
	(*GetListArrivalProductRequest)(nil),  // 3: arrival_service.GetListArrivalProductRequest
	(*UpdateArrivalProduct)(nil),          // 4: arrival_service.UpdateArrivalProduct
	(*ArrivalProduct)(nil),                // 5: arrival_service.ArrivalProduct
	(*GetListArrivalProductResponse)(nil), // 6: arrival_service.GetListArrivalProductResponse
}
var file_arrival_product_service_proto_depIdxs = []int32{
	1, // 0: arrival_service.ArrivalProductService.Create:input_type -> arrival_service.CreateArrivalProduct
	2, // 1: arrival_service.ArrivalProductService.GetByID:input_type -> arrival_service.ArrivalProductPrimaryKey
	3, // 2: arrival_service.ArrivalProductService.GetList:input_type -> arrival_service.GetListArrivalProductRequest
	4, // 3: arrival_service.ArrivalProductService.Update:input_type -> arrival_service.UpdateArrivalProduct
	2, // 4: arrival_service.ArrivalProductService.Delete:input_type -> arrival_service.ArrivalProductPrimaryKey
	5, // 5: arrival_service.ArrivalProductService.Create:output_type -> arrival_service.ArrivalProduct
	5, // 6: arrival_service.ArrivalProductService.GetByID:output_type -> arrival_service.ArrivalProduct
	6, // 7: arrival_service.ArrivalProductService.GetList:output_type -> arrival_service.GetListArrivalProductResponse
	5, // 8: arrival_service.ArrivalProductService.Update:output_type -> arrival_service.ArrivalProduct
	0, // 9: arrival_service.ArrivalProductService.Delete:output_type -> arrival_service.ArrivalProductEmpty
	5, // [5:10] is the sub-list for method output_type
	0, // [0:5] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_arrival_product_service_proto_init() }
func file_arrival_product_service_proto_init() {
	if File_arrival_product_service_proto != nil {
		return
	}
	file_arrival_product_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_arrival_product_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ArrivalProductEmpty); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_arrival_product_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_arrival_product_service_proto_goTypes,
		DependencyIndexes: file_arrival_product_service_proto_depIdxs,
		MessageInfos:      file_arrival_product_service_proto_msgTypes,
	}.Build()
	File_arrival_product_service_proto = out.File
	file_arrival_product_service_proto_rawDesc = nil
	file_arrival_product_service_proto_goTypes = nil
	file_arrival_product_service_proto_depIdxs = nil
}
